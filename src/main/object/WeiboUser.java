package main.object;

import main.manager.FriendshipManager;
import main.manager.UserManager;
import weibo4j.model.User;
import weibo4j.model.WeiboException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WeiboUser {
	private String uid = null;
	private String name = null;
	private String location = null;
	private Integer numberOfFollowers = null;
	private Integer numberofFriends = null;
	private Integer numberOfStatuses = null;

	private List<WeiboUser> friendList = null;

	private UserManager userManager = null;
	private FriendshipManager friendshipManager = null;

	public void WeiboUser() {
		userManager = UserManager.getInstance();
		friendshipManager = FriendshipManager.getInstance();
	}

	public WeiboUser(String uid) {
//		System.out.println("当前用户的名字：" + user.getName());
//		System.out.println("当前用户所发的微博数：" + user.getStatusesCount());
//		System.out.println("当前用户关注数：" + user.getFriendsCount());
//		System.out.println("当前用户粉丝数：" + user.getFollowersCount());
		try {
			User user = userManager.showUserById(uid); // default 'User' class provided by the SDK
			this.uid = user.getId();
			this.name = user.getName();
			this.location = user.getLocation();
			this.numberofFriends = user.getFriendsCount();
			this.numberOfFollowers = user.getFollowersCount();
			this.numberOfStatuses = user.getStatusesCount();
		} catch (WeiboException e) {
			e.printStackTrace();
		}
	}

	public String getUserInfo() {
		return "用户ID: " + uid + " | 用户名: " + name + " | 所发微博数: " + numberOfStatuses
				       + " | 关注数: " + numberofFriends + " | 粉丝数: " + numberOfFollowers;
	}

	public List<String> getFriendUidList() {
		try {
			/*
			 新浪的通知：“为进一步保护用户数据，即日起微博开放平台将对用户关系读取类接口进行升级，各接口最多返回指定用户关注数/粉丝数30%的数据。
			 本次调整涉及所有获取粉丝或粉丝id、关注列表或关注id列表接口。”
			 */
			return Arrays.asList(friendshipManager.getFriendsIdsByUid(uid));
		} catch (WeiboException e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<String> getFriendUidList(int topK) {
		try {
			/*
			 新浪的通知：“为进一步保护用户数据，即日起微博开放平台将对用户关系读取类接口进行升级，各接口最多返回指定用户关注数/粉丝数30%的数据。
			 本次调整涉及所有获取粉丝或粉丝id、关注列表或关注id列表接口。”
			 */
			return Arrays.asList(friendshipManager.getFriendsIdsByUid(uid)).subList(0, topK);
		} catch (WeiboException e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<String> getFollowerUidList() {
		try {
			/*
			 新浪的通知：“为进一步保护用户数据，即日起微博开放平台将对用户关系读取类接口进行升级，各接口最多返回指定用户关注数/粉丝数30%的数据。
			 本次调整涉及所有获取粉丝或粉丝id、关注列表或关注id列表接口。”
			 */
			return Arrays.asList(friendshipManager.getFollowersIdsById(uid));
		} catch (WeiboException e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<String> getFollowerUidList(int topK) {
		try {
			/*
			 新浪的通知：“为进一步保护用户数据，即日起微博开放平台将对用户关系读取类接口进行升级，各接口最多返回指定用户关注数/粉丝数30%的数据。
			 本次调整涉及所有获取粉丝或粉丝id、关注列表或关注id列表接口。”
			 */
			return Arrays.asList(friendshipManager.getFollowersIdsById(uid)).subList(0, topK);
		} catch (WeiboException e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<WeiboUser> getFriendList(int topK) {
		List<String> friendUidList = getFriendUidList(topK);
		List<WeiboUser> friendList = new ArrayList<WeiboUser>();
		for (String uid : friendUidList) {
			friendList.add(new WeiboUser(uid));
		}
		return friendList;
	}

	public List<WeiboPost> getPostList() {
		/*
		todo: Use paging to access posts in multiple pages
		e.g., StatusWapper wrapper = tm.getUserTimelineByUid ("1450317544", new Paging(1), 0, 0)
		*/
		return null;
	}

	public List<WeiboPost> getPostList(int topK) {
		return null;
	}
}
