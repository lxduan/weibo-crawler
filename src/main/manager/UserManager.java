package main.manager;

import weibo4j.Weibo;
import weibo4j.model.PostParameter;
import weibo4j.model.User;
import weibo4j.model.WeiboException;
import weibo4j.util.WeiboConfig;

public class UserManager extends Weibo {
	private static String accessToken = "2.009q4JaB0boMkW8e5efb98210m4BKY";
	private static UserManager instance = null;

	private UserManager() {
	}

	public static UserManager getInstance() {
		if (instance == null) {
			instance = new UserManager();
			instance.setToken(accessToken);
		}
		return instance;
	}

	/**
	 * 根据用户ID获取用户信息
	 *
	 * @param uid 需要查询的用户ID
	 * @return User
	 * @throws WeiboException when Weibo service or network is unavailable
	 * @version weibo4j-V2 1.0.1
	 * @see <a href="http://open.weibo.com/wiki/2/users/show">users/show</a>
	 * @since JDK 1.5
	 */
	public User showUserById(String uid) throws WeiboException {
		return new User(client.get(WeiboConfig.getValue("baseURL") + "users/show.json",
				                          new PostParameter[]{new PostParameter("uid", uid)}).asJSONObject());
	}
}
