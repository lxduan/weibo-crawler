package main.manager;

import weibo4j.Weibo;
import weibo4j.model.PostParameter;
import weibo4j.model.User;
import weibo4j.model.WeiboException;
import weibo4j.util.WeiboConfig;

public class FriendshipManager extends Weibo {
	private static String accessToken = "2.009q4JaB0boMkW8e5efb98210m4BKY";
	private static FriendshipManager instance = null;

	private FriendshipManager() {
	}

	public static FriendshipManager getInstance() {
		if (instance == null) {
			instance = new FriendshipManager();
			instance.setToken(accessToken);
		}
		return instance;
	}

	/**
	 * 获取用户关注的用户UID列表
	 *
	 * @param uid 需要查询的用户UID
	 * @return ids
	 * @throws WeiboException when Weibo service or network is unavailable
	 * @version weibo4j-V2 1.0.0
	 * @see <a
	 * href="http://open.weibo.com/wiki/2/friendships/friends/ids">friendships/friends/ids</a>
	 * @since JDK 1.5
	 */
	public String[] getFriendsIdsByUid(String uid) throws WeiboException {
		return User.constructIds(client.get(WeiboConfig.getValue("baseURL")
				                                    + "friendships/friends/ids.json",
				                                   new PostParameter[]{new PostParameter("uid", uid)}));
	}

	/**
	 * 获取用户关注的用户UID列表
	 *
	 * @param uid    需要查询的用户UID
	 * @param count  单页返回的记录条数，默认为500，最大不超过5000
	 * @param cursor 返回结果的游标，下一页用返回值里的next_cursor，上一页用previous_cursor，默认为0
	 * @return ids
	 * @throws WeiboException when Weibo service or network is unavailable
	 * @version weibo4j-V2 1.0.0
	 * @see <a
	 * href="http://open.weibo.com/wiki/2/friendships/friends/ids">friendships/friends/ids</a>
	 * @since JDK 1.5
	 */
	public String[] getFriendsIdsByUid(String uid, Integer count, Integer cursor)
			throws WeiboException {
		return User.constructIds(client.get(WeiboConfig.getValue("baseURL") + "friendships/friends/ids.json",
				                                   new PostParameter[]{new PostParameter("uid", uid),
						                                                      new PostParameter("count", count.toString()),
						                                                      new PostParameter("cursor", cursor.toString())}));
	}

	/**
	 * 获取用户粉丝的用户UID列表
	 *
	 * @param uid 需要查询的用户ID
	 * @return list of users
	 * @throws WeiboException when Weibo service or network is unavailable
	 * @version weibo4j-V2 1.0.0
	 * @see <a
	 * href="http://open.weibo.com/wiki/2/friendships/followers/ids">friendships/followers/ids</a>
	 * @since JDK 1.5
	 */
	public String[] getFollowersIdsById(String uid) throws WeiboException {
		return User.constructIds(client.get(WeiboConfig.getValue("baseURL")
				                                    + "friendships/followers/ids.json",
				                                   new PostParameter[]{new PostParameter("uid", uid)}));
	}

	/**
	 * 获取用户粉丝的用户UID列表
	 *
	 * @param uid    需要查询的用户ID
	 * @param count  单页返回的记录条数，默认为500，最大不超过5000
	 * @param cursor 返回结果的游标，下一页用返回值里的next_cursor，上一页用previous_cursor，默认为0
	 * @return list of users
	 * @throws WeiboException when Weibo service or network is unavailable
	 * @version weibo4j-V2 1.0.0
	 * @see <a
	 * href="http://open.weibo.com/wiki/2/friendships/followers/ids">friendships/followers/ids</a>
	 * @since JDK 1.5
	 */
	public String[] getFollowersIdsById(String uid, Integer count,
	                                    Integer cursor) throws WeiboException {
		return User.constructIds(client.get(WeiboConfig.getValue("baseURL")
				                                    + "friendships/followers/ids.json",
				                                   new PostParameter[]{new PostParameter("uid", uid),
						                                                      new PostParameter("count", count.toString()),
						                                                      new PostParameter("cursor", cursor.toString())}));
	}

}
