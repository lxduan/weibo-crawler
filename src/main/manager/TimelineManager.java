package main.manager;

import weibo4j.Weibo;
import weibo4j.model.PostParameter;
import weibo4j.model.WeiboException;
import weibo4j.org.json.JSONObject;
import weibo4j.util.WeiboConfig;

public class TimelineManager extends Weibo {
	private static String accessToken = "2.009q4JaB0boMkW8e5efb98210m4BKY";
	private static TimelineManager instance = null;

	private TimelineManager() {
	}

	public static TimelineManager getInstance() {
		if (instance == null) {
			instance = new TimelineManager();
			instance.setToken(accessToken);
		}
		return instance;
	}

	/**
	 * 获取某个用户最新发表的微博列表ID
	 *
	 * @return user_timeline IDS
	 * @throws weibo4j.model.WeiboException when Weibo service or network is unavailable
	 * @version weibo4j-V2 1.0.1
	 * @see <a
	 * href="http://open.weibo.com/wiki/2/statuses/user_timeline">statuses/user_timeline</a>
	 * @since JDK 1.5
	 */
	public JSONObject getUserTimelineIdsByUid(String uid) throws WeiboException {
		return client.get(WeiboConfig.getValue("baseURL") + "statuses/user_timeline/ids.json",
				                 new PostParameter[]{new PostParameter("uid", uid)}).asJSONObject();
	}
}
